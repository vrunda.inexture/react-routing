import React, { Fragment, useContext } from 'react';

import AuthContext from '../context/AuthContext';

const Dashboard = () => {
  let authContext = useContext(AuthContext);
  let { currentUser } = authContext;

  // usign state value of logged in user
  return (
    <Fragment>
      <section>
        <div className='container py-5 h-100'>
          <div className='row d-flex justify-content-center align-items-center h-100'>
            <h4 className='text-center my-5'>
              Hey, {currentUser?.username} Welcome to Dashboard.
            </h4>
            <div className='col-md-8'>
              <div className='card' style={{ borderRadius: '15px' }}>
                <div className='card-body text-center'>
                  <div className='mt-3 mb-4'>
                    <img
                      src={
                        currentUser?.gender === 'Female'
                          ? 'https://mdbcdn.b-cdn.net/img/Photos/new-templates/bootstrap-chat/ava1-bg.webp'
                          : 'https://img.icons8.com/color/800/000000/user-male-circle--v1.png'
                      }
                      className='rounded-circle img-fluid w-25'
                      alt='img-1'
                    />
                  </div>
                  <h4 className='mb-2'>
                    <i className='fa-solid fa-at fs-5'></i>
                    {currentUser?.username}
                  </h4>
                  <p className='text-muted mb-4'>
                    <i className='fa-solid fa-phone-flip me-1'></i>
                    {currentUser?.phone}
                    <span className='mx-2'>|</span>
                    <i className='fa-solid fa-envelope me-1'></i>
                    <a href='#!' className='text-decoration-none text-dark'>
                      {currentUser?.email}
                    </a>
                  </p>

                  <div className='d-flex justify-content-around text-center mt-5 mb-2'>
                    <div>
                      <p className='mb-2 h5'>Gender</p>
                      <p className='text-muted mb-0'>{currentUser?.gender}</p>
                    </div>
                    <div className='px-3'>
                      <p className='mb-2 h5'>City</p>
                      <p className='text-muted mb-0'>{currentUser?.city}</p>
                    </div>
                    <div>
                      <p className='mb-2 h5'>State</p>
                      <p className='text-muted mb-0'>{currentUser?.state}</p>
                    </div>
                  </div>
                </div>
              </div>
            </div>
          </div>
        </div>
      </section>
    </Fragment>
  );
};

export default Dashboard;
