const AuthReducer = (state, action) => {
  switch (action.type) {
    case 'SET_DATA':
      return {
        ...state,
        users: action.payload.decryptedUsers, //from local storage to state
        currentUser: action.payload.decryptedUser,
      };

    case 'SIGN_UP':
      return {
        ...state,
        users: [...state.users, action.payload], //append to the list
      };

    case 'SIGN_IN':
      return {
        ...state,
        currentUser: action.payload, //logged in user
      };

    case 'SIGN_OUT':
      return {
        ...state,
        currentUser: null, //logged out
      };

    default:
      return { ...state };
  }
};

export default AuthReducer;
