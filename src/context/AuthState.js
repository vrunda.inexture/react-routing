import React, { useReducer } from 'react';
import AuthContext from './AuthContext';
import AuthReducer from './AuthReducer';

import CryptoJS from 'crypto-js';

const AuthState = (props) => {
  const initialState = {
    users: [],
    currentUser: null,
  };

  const [state, dispatch] = useReducer(AuthReducer, initialState);

  // get data from local storage
  const getData = () => {
    let users = localStorage.getItem('users');
    let user = localStorage.getItem('user');

    // case-1 : no user has registered
    if (!users) {
      return;
    }

    // after registering decrypt the data
    var bytes = CryptoJS.AES.decrypt(users, 'secret key 123');
    var decryptedUsers = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    var decryptedUser = null;

    //case-2: user is registered, but not logged in
    if (user) {
      var bytes = CryptoJS.AES.decrypt(user, 'secret key 123');
      decryptedUser = JSON.parse(bytes.toString(CryptoJS.enc.Utf8));
    }

    // store data in state from local storage
    dispatch({
      type: 'SET_DATA',
      payload: { decryptedUsers, decryptedUser },
    });
  };

  // save data after sign up in state
  const signUp = (user) => {
    dispatch({
      type: 'SIGN_UP',
      payload: user,
    });

    // encrypt the data
    var users = CryptoJS.AES.encrypt(
      JSON.stringify([...state.users, user]),
      'secret key 123'
    ).toString();

    // storing in localstorage
    localStorage.setItem('users', users);
  };

  // sign in
  const signIn = (user) => {
    // find the user from logged in users list
    let authenticatedUser = state.users.find(
      (person) =>
        person.email === user.email && person.password === user.password
    );

    // save data in state
    if (authenticatedUser) {
      dispatch({
        type: 'SIGN_IN',
        payload: authenticatedUser,
      });

      // encrypt the data
      var encryptedUser = CryptoJS.AES.encrypt(
        JSON.stringify(authenticatedUser),
        'secret key 123'
      ).toString();

      // store in local storage
      localStorage.setItem('user', encryptedUser);
    } else {
      // user not found
      window.alert('Invalid Credentials!');
    }
  };

  // sign out
  const signOut = () => {
    localStorage.removeItem('user');
    dispatch({
      type: 'SIGN_OUT',
    });
  };

  return (
    // using context
    <AuthContext.Provider
      value={{
        currentUser: state.currentUser,
        users: state.users,
        signUp,
        signIn,
        signOut,
        getData,
      }}
    >
      {props.children}
    </AuthContext.Provider>
  );
};

export default AuthState;
