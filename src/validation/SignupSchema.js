import * as yup from 'yup';

// validaton schema for sign up form
export const SignUpSchema = yup.object({
  username: yup
    .string()
    .required()
    .matches(/^[A-Za-z]+$/, 'Username can only contain letters')
    .min(3, 'Name must be at least 3 characters long'),

  email: yup.string().required().email(),

  phone: yup
    .string()
    .required()
    .matches(/^[0-9]+$/, 'Phone number must be only digits')
    .min(10, 'Must be exactly 10 digits')
    .max(10),

  gender: yup.string().required('gender is required').nullable(),

  city: yup.string().required(),

  state: yup.string().required(),

  password: yup
    .string()
    .required('Please enter your password')
    .matches(
      /^.*(?=.{8,})((?=.*[!@#$%^&*()\-_=+{};:,<.>]){1})(?=.*\d)((?=.*[a-z]){1})((?=.*[A-Z]){1}).*$/,
      'Password must contain at least 8 characters, one uppercase, one number and one special case character'
    ),

  confirmPassword: yup
    .string()
    .required('Please confirm your password')
    .oneOf([yup.ref('password'), null], 'Passwords must match'),
});
