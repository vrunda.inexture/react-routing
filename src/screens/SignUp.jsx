import React, { useContext, useEffect } from 'react';
import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { SignUpSchema } from '../validation/SignupSchema';
import { Form, Button, Container } from 'react-bootstrap';
import AuthContext from '../context/AuthContext';
import { useNavigate } from 'react-router-dom';

//signup
const SignUp = () => {
  let authContext = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (authContext.currentUser) {
      navigate('/dashboard');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [authContext.currentUser]);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(SignUpSchema),
    mode: 'onChange',
  });

  // redirecting to signin after saving user data
  const onSubmit = (data) => {
    authContext.signUp(data);
    navigate('/signin');
  };

  return (
    <div className='my-5 m-auto w-50'>
      <Container className='w-100 bg-light p-5 rounded'>
        <h1> Sign Up</h1>
        <hr />
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className='mb-3'>
            <Form.Label>Username</Form.Label>
            <Form.Control
              {...register('username')}
              type='text'
              className='form-control'
              placeholder='Enter you Username'
            />
            <Form.Text className='text-danger'>
              {errors.username?.message}
            </Form.Text>
          </Form.Group>
          <Form.Group className='mb-3'>
            <Form.Label>Email </Form.Label>
            <Form.Control
              {...register('email')}
              className='form-control'
              placeholder='name@example.com'
              type='text'
            />
            <Form.Text className='text-danger'>
              {errors.email?.message}
            </Form.Text>
          </Form.Group>
          <Form.Group className='mb-3'>
            <Form.Label>Phone</Form.Label>
            <Form.Control
              {...register('phone')}
              type='text'
              className='form-control'
              placeholder='Enter your phone'
            />
            <Form.Text className='text-danger'>
              {errors.phone?.message}
            </Form.Text>
          </Form.Group>
          <Form.Group className='mb-3'>
            <Form.Label>Gender</Form.Label>
            <br />
            <Form.Check
              {...register('gender')}
              inline
              label='Female'
              name='gender'
              type='radio'
              value='Female'
            />
            <Form.Check
              {...register('gender')}
              inline
              label='Male'
              name='gender'
              type='radio'
              value='Male'
            />
            <Form.Text className='text-danger'>
              {errors.gender?.message}
            </Form.Text>
          </Form.Group>
          <Form.Group className='mb-3'>
            <Form.Select className='form-select' {...register('state')}>
              <option value=''>Select your state</option>
              <option value='gujarat'>Gujarat</option>
              <option value='maharashtra'>Maharashtra</option>
              <option value='punjab'>Punjab</option>
              <option value='Rajasthan'>Rajasthan</option>
            </Form.Select>
            <Form.Text className='text-danger'>
              {errors.city?.message}
            </Form.Text>
          </Form.Group>
          <Form.Group className='mb-3'>
            <Form.Select className='form-select' {...register('city')}>
              <option value=''>Select your city</option>
              <option value='dakor'>Dakor</option>
              <option value='modasa'>Modasa</option>
              <option value='anand'>Anand</option>
              <option value='ahmedabad'>Ahmedabad</option>
            </Form.Select>
            <Form.Text className='text-danger'>
              {errors.city?.message}
            </Form.Text>
          </Form.Group>
          <Form.Group className='mb-3'>
            <Form.Label>Password</Form.Label>
            <Form.Control
              {...register('password')}
              type='text'
              className='form-control'
              placeholder='Enter your password'
            />
            <Form.Text className='text-danger'>
              {errors.password?.message}
            </Form.Text>
          </Form.Group>
          <Form.Group className='mb-3'>
            <Form.Label>Confirm Password</Form.Label>
            <Form.Control
              {...register('confirmPassword')}
              type='text'
              className='form-control'
              placeholder='Enter your password again'
            />
            <Form.Text className='text-danger'>
              {errors.confirmPassword?.message}
            </Form.Text>
          </Form.Group>
          <Form.Text className='text-danger'>{errors.msg?.message}</Form.Text>
          <Button type='submit' className='btn btn-primary' variant='primary'>
            Sign Up
          </Button>{' '}
        </Form>
      </Container>
    </div>
  );
};

export default SignUp;
