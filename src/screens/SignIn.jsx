import React, { useContext, useEffect } from 'react';

import { useForm } from 'react-hook-form';
import { yupResolver } from '@hookform/resolvers/yup';
import { SignInSchema } from '../validation/SignInSchema';
import { Form, Button, Container } from 'react-bootstrap';

import AuthContext from '../context/AuthContext';
import { useNavigate } from 'react-router-dom';

const SignIn = () => {
  let authContext = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (authContext.currentUser) {
      navigate('/dashboard');
    }
    // eslint-disable-next-line react-hooks/exhaustive-deps
  }, [authContext.currentUser]);

  const {
    register,
    handleSubmit,
    formState: { errors },
  } = useForm({
    resolver: yupResolver(SignInSchema),
    mode: 'onChange',
  });

  // redirecting to dashboard after saving data
  const onSubmit = (data) => {
    authContext.signIn(data);
    navigate('/dashboard');
  };

  return (
    <div className='my-5 m-auto w-50'>
      <Container className='w-100 bg-light p-5 rounded'>
        <h1> Sign In</h1>
        <hr />
        <Form onSubmit={handleSubmit(onSubmit)}>
          <Form.Group className='mb-3'>
            <Form.Label>Email </Form.Label>
            <Form.Control
              {...register('email')}
              className='form-control'
              placeholder='name@example.com'
              type='text'
            />
            <Form.Text className='text-danger'>
              {errors.email?.message}
            </Form.Text>
          </Form.Group>
          <Form.Group className='mb-3'>
            <Form.Label>Password</Form.Label>
            <Form.Control
              {...register('password')}
              type='text'
              className='form-control'
              placeholder='Enter your password'
            />
            <Form.Text className='text-danger'>
              {errors.password?.message}
            </Form.Text>
          </Form.Group>
          <Form.Text className='text-danger'>{errors.msg?.message}</Form.Text>
          <Button type='submit' className='btn btn-primary' variant='primary'>
            Sign In
          </Button>{' '}
        </Form>
      </Container>
    </div>
  );
};

export default SignIn;
