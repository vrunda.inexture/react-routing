import React, { useContext, useEffect } from 'react';
import AuthContext from '../context/AuthContext';
import { useNavigate } from 'react-router-dom';

const Home = () => {
  let authContext = useContext(AuthContext);
  const navigate = useNavigate();

  useEffect(() => {
    if (authContext.currentUser) {
      navigate('/dashboard');
    }
  }, [authContext.currentUser]);

  return <h2 className='text-center mt-5'> Home</h2>;
};

export default Home;
