import React from 'react';
import { BrowserRouter, Routes, Route } from 'react-router-dom';
import SignIn from './screens/SignIn';
import SignUp from './screens/SignUp';
import Dashboard from './protectedScreen/Dashboard';
import Header from './components/Header';
import Home from './screens/Home';
import PrivateRoute from './components/PrivateRoute';
import PageNotFound from './components/PageNotFound';

const Router = () => {
  return (
    <BrowserRouter>
      <Header />
      <Routes>
        <Route path='/' element={<Home />} />
        <Route path='signin' element={<SignIn />} />
        <Route path='signup' element={<SignUp />} />
        <Route element={<PrivateRoute />}>
          <Route path='dashboard' element={<Dashboard />} />
        </Route>
        <Route path='*' element={<PageNotFound />} />
      </Routes>
    </BrowserRouter>
  );
};

export default Router;
