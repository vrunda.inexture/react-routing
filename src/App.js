import React from 'react';
import Router from './Router';
import AuthState from './context/AuthState';

const App = () => {
  return (
    // gloabl state
    <AuthState>
      <Router />
    </AuthState>
  );
};

export default App;
