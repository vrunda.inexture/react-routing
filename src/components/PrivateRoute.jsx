import React from 'react';
import { Navigate, Outlet } from 'react-router-dom';

const PrivateRoute = () => {
  // finding logged in user
  let user = localStorage.getItem('user');

  // dashboard or sign in page
  return user ? <Outlet /> : <Navigate to='/signin' />;
};

export default PrivateRoute;
