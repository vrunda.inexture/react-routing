import React, { useContext, useEffect } from 'react';
import { Outlet } from 'react-router-dom';
import { Navbar, Container, Nav } from 'react-bootstrap';
import { LinkContainer } from 'react-router-bootstrap';
import AuthContext from '../context/AuthContext';
import { useNavigate } from 'react-router-dom';

const Header = () => {
  let authContext = useContext(AuthContext);
  let { currentUser, getData } = authContext;
  const navigate = useNavigate();

  // calling get data to store in state
  useEffect(() => {
    getData();
  }, []);

  const handleClick = () => {
    authContext.signOut();
    navigate('/signin');
  };
  return (
    <>
      <Navbar bg='light' expand='md'>
        <Container>
          <Navbar.Brand>NodeReads</Navbar.Brand>
          <Navbar.Toggle aria-controls='basic-navbar-nav' />
          <Navbar.Collapse id='basic-navbar-nav'>
            <Nav className='ms-auto'>
              {currentUser ? (
                <>
                  <LinkContainer to='/dashboard'>
                    <Nav.Link>Dashboard</Nav.Link>
                  </LinkContainer>
                  <Nav.Link onClick={handleClick}>Logout</Nav.Link>
                </>
              ) : (
                <>
                  <LinkContainer to='/'>
                    <Nav.Link>Home</Nav.Link>
                  </LinkContainer>
                  <LinkContainer to='/signin'>
                    <Nav.Link>Sign In</Nav.Link>
                  </LinkContainer>
                  <LinkContainer to='/signup'>
                    <Nav.Link>Sign Up</Nav.Link>
                  </LinkContainer>
                </>
              )}
            </Nav>
          </Navbar.Collapse>
        </Container>
      </Navbar>
      <Outlet />
    </>
  );
};

export default Header;
