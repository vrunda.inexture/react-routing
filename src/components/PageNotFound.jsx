import React from 'react';

const PageNotFound = () => {
  return (
    <div className='text-center mt-5'>
      <h1>Page not found.</h1>
      <p>Sorry, this path is not availabale.</p>
    </div>
  );
};

export default PageNotFound;
